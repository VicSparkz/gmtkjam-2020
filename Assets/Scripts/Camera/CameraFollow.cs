﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
	public Transform target;
	public Vector3 offset;

	private float rightBound;
	private float leftBound;
	private float topBound;
	private float bottomBound;
	private Vector3 pos;
	private SpriteRenderer spriteBounds;

	float vertExtent;
	float horzExtent;

	private Camera camera;

	private void Awake()
	{
		camera = GetComponent<Camera>();

		vertExtent = camera.orthographicSize;
		horzExtent = vertExtent * Screen.width / Screen.height;
	}

	private void Update()
	{
		var pos = new Vector3(target.position.x + offset.x, target.position.y + offset.y, transform.position.z + offset.z);
		//pos.x = Mathf.Clamp(pos.x, leftBound, rightBound);
		//pos.y = Mathf.Clamp(pos.y, bottomBound, topBound);
		transform.position = pos;
	}

	public void SetBounds(CameraAnchor cameraAnchor)
	{
		leftBound = (float)(cameraAnchor.transform.position.x + horzExtent - cameraAnchor.boxCollider.bounds.size.x / 2.0f);
		rightBound = (float)(cameraAnchor.transform.position.x + cameraAnchor.boxCollider.bounds.size.x / 2.0f - horzExtent);
		bottomBound = (float)(cameraAnchor.transform.position.y + vertExtent - cameraAnchor.boxCollider.bounds.size.y / 2.0f);
		topBound = (float)(cameraAnchor.transform.position.y + cameraAnchor.boxCollider.bounds.size.y / 2.0f - vertExtent);
	}
}
