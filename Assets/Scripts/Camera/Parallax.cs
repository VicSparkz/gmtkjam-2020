﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
	public GameObject camera;
	public Vector2 parallaxEffect;

	private float length, startPosX, startPosY, startCameraPosX, startCameraPosY;

    private void Start()
    {
		startPosX = transform.position.x;
		startPosY = transform.position.y;
		length = GetComponent<SpriteRenderer>().bounds.size.x;

		startCameraPosX = camera.transform.position.x;
		startCameraPosY = camera.transform.position.y;

	}

    private void Update()
    {
		float tempX = ((camera.transform.position.x - startCameraPosX) * (1 - parallaxEffect.x));
		float distX = ((camera.transform.position.x - startCameraPosX) * parallaxEffect.x);
		float tempY = ((camera.transform.position.y - startCameraPosY) * (1 - parallaxEffect.y));
		float distY = ((camera.transform.position.y - startCameraPosY) * parallaxEffect.y);

		transform.position = new Vector3(startPosX + distX, startPosY + distY, transform.position.z);

		if (tempX > startPosX + length)
		{
			startPosX += length;
		}
		else if (tempX < startPosX - length)
		{
			startPosX -= length;
		}
	}
}
