﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerActions
{
	jump = 0,
	hookshot = 1,
	moveLeft = 2,
	moveRight = 3
}

public class GameEvents
{
	public static Action<int> nextScreen;
	public static Action<PlayerActions, int> setEnergyAmount;
	public static Action<PlayerActions, int> playerAction;
	public static Action resetToCheckpoint;
	public static Action playCredits;
	public static Action rustPlayer;
}
