﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public GameObject camera;
	public GameObject[] cameraAnchors;

	private float transitionSpeed = 0.5f;

	private int currentAnchor = 0;

	private void OnEnable()
	{
		GameEvents.nextScreen += NextScreen;
	}

	private void OnDisable()
	{
		GameEvents.nextScreen -= NextScreen;
	}
    private void NextScreen(int index)
	{
		Debug.Log("Next Screen");

		currentAnchor = index;

	}
	void Update()
	{
		//camera.transform.position = Vector3.Lerp(cameraAnchors[currentAnchor].transform.position, camera.transform.position, transitionSpeed) + Vector3.back * 10; 
	}
}
