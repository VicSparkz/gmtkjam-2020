﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveableTarget : Target
{
	private Hookshot hookshot;

	private bool grounded;
	private Vector2 velocity = new Vector2();
	private Vector3 startPosition;

	protected override void Start()
	{
		base.Start();

		startPosition = transform.position;
	}

	private void OnEnable()
	{
		GameEvents.resetToCheckpoint += Reset;
	}

	private void OnDisable()
	{
		GameEvents.resetToCheckpoint -= Reset;
	}

	private void Reset()
	{
		transform.position = startPosition;
	}

	private void Update()
	{
		if (grounded)
		{
			velocity.y = 0;	
		}

		if (hookshot != null && grounded)
		{
			velocity.x = hookshot.speed * hookshot.direction.x;
		}
		else
		{
			hookshot = null;

			velocity.x = 0;
		}

		velocity.y += Physics2D.gravity.y * Time.deltaTime;

		transform.Translate(velocity * Time.deltaTime);

		grounded = false;

		Collider2D[] hits = Physics2D.OverlapBoxAll(transform.position, boxCollider.size, 0);

		foreach (Collider2D hit in hits)
		{
			if (hit == boxCollider || hit.isTrigger)
				continue;

			ColliderDistance2D colliderDistance = hit.Distance(boxCollider);
			if (colliderDistance.isOverlapped)
			{
				transform.Translate(colliderDistance.pointA - colliderDistance.pointB);

				if (Vector2.Angle(colliderDistance.normal, Vector2.up) < 90 && velocity.y < 0)
				{
					grounded = true;
				}
			}
		}
	}

	public override void OnHit(Hookshot hookshot)
	{
		this.hookshot = hookshot;
	}
}
