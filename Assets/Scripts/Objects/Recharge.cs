﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recharge : MonoBehaviour
{
	public int jumpEnergy;
	public int hookshotEnergy;
	public int moveLeftEnergy;
	public int moveRightEnergy;
	public Sprite depleatedSprite;

	private BoxCollider2D boxCollider;

	private void Start()
	{
		boxCollider = GetComponent<BoxCollider2D>();
	}

	private void OnEnable()
	{
		GameEvents.resetToCheckpoint += ResetCheckpoint;
	}

	private void OnDisable()
	{
		GameEvents.resetToCheckpoint -= ResetCheckpoint;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		GameEvents.setEnergyAmount(PlayerActions.jump, jumpEnergy);
		GameEvents.setEnergyAmount(PlayerActions.hookshot, hookshotEnergy);
		GameEvents.setEnergyAmount(PlayerActions.moveLeft, moveLeftEnergy);
		GameEvents.setEnergyAmount(PlayerActions.moveRight, moveRightEnergy);

		GetComponent<SpriteRenderer>().sprite = depleatedSprite;

		boxCollider.enabled = false;
	}

	private void ResetCheckpoint()
	{
		boxCollider.enabled = true;
	}
}
