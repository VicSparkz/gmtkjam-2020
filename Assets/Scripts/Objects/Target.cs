﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
	protected BoxCollider2D boxCollider;

	protected virtual void Start()
	{
		boxCollider = GetComponent<BoxCollider2D>();
	}

	public virtual void OnHit(Hookshot hookshot)
	{

	}
}
