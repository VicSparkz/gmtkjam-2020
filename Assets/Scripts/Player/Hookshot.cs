﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hookshot : MonoBehaviour
{
	public Player player;
    public Transform arm;

	[HideInInspector]
	public float speed = 25;
	[HideInInspector]
	public Vector2 direction;

	private float maxDistance = 15;

	private float startX;
	private bool returning;
	private bool attached;

	private AudioSource audioSource;

	private BoxCollider2D boxCollider;

    //private float armAnchor;

    private void Start()
    {
		boxCollider = GetComponent<BoxCollider2D>();
		audioSource = GetComponent<AudioSource>();

	}
	
    private void Update()
    {
		transform.Translate(direction * speed * Time.deltaTime);

        arm.localScale = new Vector3(Mathf.Abs(player.transform.position.x - transform.position.x)-.6f,1,1);

        if (!returning)
		{
			Collider2D[] hits = Physics2D.OverlapBoxAll(transform.position, boxCollider.size, 0);

			foreach (Collider2D hit in hits)
			{
				if (hit == boxCollider)
					continue;

				ColliderDistance2D colliderDistance = hit.Distance(boxCollider);
				if (colliderDistance.isOverlapped)
				{
					if (hit.CompareTag(UnityConstants.Tags.Target) && !attached)
					{
						hit.GetComponent<Target>().OnHit(this);
                    
                        direction = Vector2.zero;             

                        StartCoroutine(player.HookshotAttachedPullPlayer(transform));

						attached = true;
					}
					else if (hit.CompareTag(UnityConstants.Tags.MoveableTarget) && !attached)
					{
						hit.GetComponent<MoveableTarget>().OnHit(this);

						direction *= -1;
						returning = true;
						attached = true;
					}

					audioSource.Play();
				}
			}

			if (!returning && Mathf.Abs(startX - transform.position.x) > maxDistance)
			{
				direction *= -1;
				returning = true;
			}
		}

		if(returning && Mathf.Abs(startX - transform.position.x) < 0.5f)
		{
			player.HookshotFinished();
			Destroy(gameObject);
		}
	}

	public void Shoot(Player player)
	{
		this.player = player;
		this.direction = new Vector2(player.transform.localScale.x, 0);
		this.transform.localScale = player.transform.localScale;

		transform.position = player.transform.position;

		startX = transform.position.x;
    }


}