﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
	public GameObject hookshot;
	public AudioSource runAudioSource;

	public AudioClip runClip;
	public AudioClip jumpClip;
	public AudioClip shootClip;
	public AudioClip zoomClip;
	public AudioClip checkpointClip;

	public GameObject pauseMenu;

	private float jump = 2.25f;
	private float movementSpeed = 5;
	private float movementAcceleration = 100;
	private float airAcceleration = 30;
	private float groundDeceleration = 70;

	private bool controlsEnabled = true;

	private BoxCollider2D boxCollider;
	private Animator animator;
	private AudioSource audioSource;

	private Vector2 velocity;
	private bool grounded;
	private bool grappling = false;
	private Vector2 movementValue;

	private int jumpEnergy = 6;
	private int hookshotEnergy = 6;
	private int moveLeftEnergy = 6;
	private int moveRightEnergy = 6;

	private bool stoppedMoving = true;

    private InputManager inputActions;

	public GameObject currentCheckpoint;

	private void Start()
    {
		velocity = Vector2.zero;
		boxCollider = gameObject.GetComponent<BoxCollider2D>();
		animator = GetComponent<Animator>();
		audioSource = GetComponent<AudioSource>();

		movementValue = new Vector2();

		inputActions = new InputManager();

		GameEvents.setEnergyAmount(PlayerActions.jump, 0);
		GameEvents.setEnergyAmount(PlayerActions.hookshot, 0);
		GameEvents.setEnergyAmount(PlayerActions.moveLeft, 0);
		GameEvents.setEnergyAmount(PlayerActions.moveRight, 1);
	}

	private void OnEnable()
	{
		GameEvents.setEnergyAmount += OnSetEnergyAmount;
		GameEvents.rustPlayer += OnRustPlayer;
	}

	private void OnDisable()
	{
		GameEvents.setEnergyAmount -= OnSetEnergyAmount;
		GameEvents.rustPlayer -= OnRustPlayer;
	}

	private void Update()
	{
		if (grounded)
		{
			velocity.y = 0;
		}

		float acceleration = grounded ? movementAcceleration : airAcceleration;
		float deceleration = grounded ? groundDeceleration : 0;

		if (!grappling)
		{
			if (movementValue.x != 0)
			{
				velocity.x = Mathf.MoveTowards(velocity.x, movementSpeed * movementValue.x, acceleration * Time.deltaTime);
			}
			else
			{
				velocity.x = Mathf.MoveTowards(velocity.x, 0, deceleration * Time.deltaTime);
			}
		
			velocity.y += Physics2D.gravity.y * Time.deltaTime;
		}
		else
		{
			velocity = Vector2.zero;
		}

		animator.SetFloat("x", velocity.x);
		animator.SetFloat("y", velocity.y);

		transform.Translate(velocity * Time.deltaTime);

		grounded = false;
		
		Collider2D[] hits = Physics2D.OverlapBoxAll(transform.position, boxCollider.size, 0);

		foreach (Collider2D hit in hits)
		{
			if (hit == boxCollider || hit.isTrigger)
				continue;

			ColliderDistance2D colliderDistance = hit.Distance(boxCollider);
			if (colliderDistance.isOverlapped)
			{
				transform.Translate(colliderDistance.pointA - colliderDistance.pointB);
				
				if (Vector2.Angle(colliderDistance.normal, Vector2.up) < 90 && velocity.y < 0)
				{
					grounded = true;
				}
			}
		}

		animator.SetBool("grounded", grounded);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.gameObject.CompareTag(UnityConstants.Tags.Goal))
		{
			GameEvents.nextScreen(collision.gameObject.GetComponent<Goal>().index);
		}
		else if (collision.gameObject.CompareTag(UnityConstants.Tags.Recharge))
		{
			currentCheckpoint = collision.gameObject;
			audioSource.PlayOneShot(checkpointClip);
			stoppedMoving = true;
		}
		else if (collision.gameObject.CompareTag(UnityConstants.Tags.Die))
		{
			controlsEnabled = false;
			animator.SetTrigger("die");
			velocity = Vector2.zero;
			movementValue = Vector2.zero;

			StartCoroutine(PlayCredits());
		}
	}

	private IEnumerator PlayCredits()
	{
		yield return new WaitForSeconds(1.0f);

		GameEvents.playCredits();

		yield return new WaitForSeconds(40.0f);

		Application.Quit();
	}

	private void OnRustPlayer()
	{
		animator.SetTrigger("rust");
		audioSource.PlayOneShot(checkpointClip);
	}

	public void OnJump()
	{
		if (!controlsEnabled || !grounded || jumpEnergy <= 0) return;

		Debug.Log("JUMP");
		velocity.y = Mathf.Sqrt(2 * jump * Mathf.Abs(Physics2D.gravity.y));
		grounded = false;
		animator.SetTrigger("jump");

		audioSource.PlayOneShot(jumpClip);

		jumpEnergy--;

		GameEvents.playerAction(PlayerActions.jump, jumpEnergy);
	}

	public void OnMovement(InputValue value)
	{
		if (!controlsEnabled) return;

		movementValue = value.Get<Vector2>();

		if (movementValue.x < 0 && moveLeftEnergy > 0)
		{
			transform.localScale = new Vector3(-1, 1, 1);

			animator.SetBool("moving", true);

			if(stoppedMoving)
			{
				moveLeftEnergy--;

				GameEvents.playerAction(PlayerActions.moveLeft, moveLeftEnergy);

				stoppedMoving = false;
			}
		}
		else if (movementValue.x > 0 && moveRightEnergy > 0)
		{
			transform.localScale = new Vector3(1, 1, 1);

			animator.SetBool("moving", true);

			if (stoppedMoving)
			{
				moveRightEnergy--;

				GameEvents.playerAction(PlayerActions.moveRight, moveRightEnergy);

				stoppedMoving = false;
			}
		}
		else
		{
			animator.SetBool("moving", false);
			stoppedMoving = true;

			movementValue = Vector2.zero;
		}
	}

	public void OnHookshot()
	{
		if (!controlsEnabled || !grounded || hookshotEnergy <= 0) return;

		GameObject go = Instantiate(hookshot) as GameObject;
		go.GetComponent<Hookshot>().Shoot(this);

		controlsEnabled = false;
		grappling = true;
		animator.SetBool("grapple", true);
		animator.SetBool("moving", false);
		stoppedMoving = true;
		movementValue = Vector2.zero;

		audioSource.PlayOneShot(shootClip);

		hookshotEnergy--;

		GameEvents.playerAction(PlayerActions.hookshot, hookshotEnergy);
	}

	public void OnResetToCheckpoint()
	{
		if (!controlsEnabled) return;

		transform.position = currentCheckpoint.transform.position;
		GameEvents.resetToCheckpoint();
	}

	public void OnPause()
	{
		pauseMenu.SetActive(!pauseMenu.activeSelf);
	}

	public void OnQuit()
	{
		if(pauseMenu.activeSelf)
		{
			Application.Quit();
		}
	}

	public void OnRefill()
	{
#if UNITY_EDITOR
		GameEvents.setEnergyAmount(PlayerActions.jump, 5);
		GameEvents.setEnergyAmount(PlayerActions.hookshot, 5);
		GameEvents.setEnergyAmount(PlayerActions.moveLeft, 5);
		GameEvents.setEnergyAmount(PlayerActions.moveRight, 5);
#endif
	}

	public void HookshotFinished()
	{
		controlsEnabled = true;
		grappling = false;
		animator.SetBool("grapple", false);
	}

    public IEnumerator HookshotAttachedPullPlayer(Transform hookTransform)
    {
		velocity.y = 0;

		audioSource.PlayOneShot(zoomClip);

		while (Mathf.Abs(transform.position.x - hookTransform.position.x) > 0.01f)
        {
            transform.position = new Vector3(Mathf.Lerp(transform.position.x, hookTransform.position.x, 10 * Time.deltaTime), transform.position.y, transform.position.z);       
            yield return new WaitForSeconds(0);
        }
        
        Destroy(hookTransform.gameObject);
        HookshotFinished();
	}

	private void OnSetEnergyAmount(PlayerActions playerActions, int energy)
	{
		switch (playerActions)
		{
			case PlayerActions.jump:
				jumpEnergy = energy;
				break;
			case PlayerActions.hookshot:
				hookshotEnergy = energy;
				break;
			case PlayerActions.moveLeft:
				moveLeftEnergy = energy;
				break;
			case PlayerActions.moveRight:
				moveRightEnergy = energy;
				break;
			default:
				break;
		}
	}

	public void PlayRun()
	{
		runAudioSource.loop = true;
		runAudioSource.clip = runClip;
		runAudioSource.Play();
	}

	public void StopRun()
	{
		runAudioSource.Stop();
	}
}