﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Resource : MonoBehaviour
{
	public Sprite empty;
	public List<Image> energies;

	private int currentMax;

	private void Start()
	{
		currentMax = energies.Count - 1;
	}

	public void Recharge(int amount)
	{
		for(int i = 0; i < energies.Count; i++)
		{
			energies[i].overrideSprite = empty;
			energies[i].gameObject.SetActive(i < amount);
		}

		currentMax = energies.Count - 1;

		GetComponent<Image>().enabled = amount != 0;

		if(amount != 0)
		{
			StartCoroutine(RechargeRoutine());
		}
	}

	private IEnumerator RechargeRoutine()
	{
		for (int i = 0; i < energies.Count; i++)
		{
			energies[i].overrideSprite = null;

			yield return new WaitForSeconds(0.1f);
		}
	}

	public void Deplete(int energy)
	{
		for(int i = 0; i < energies.Count; i++)
		{
			energies[i].overrideSprite = energy <= i ? empty : null;
		}
	}
}
