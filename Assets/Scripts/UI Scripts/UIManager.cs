﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
	public Resource jump;
	public Resource hookshot;
	public Resource moveLeft;
	public Resource moveRight;

	public GameObject credits;

	public AudioClip creditsClip;

	private AudioSource audioSource;

	private void Start()
	{
		audioSource = GetComponent<AudioSource>();

		audioSource.Play();
	}

	private void OnEnable()
	{
		GameEvents.playerAction += OnPlayerAction;
		GameEvents.setEnergyAmount += OnSetEnergyAmount;
		GameEvents.playCredits += OnPlayCredits;
	}

	private void OnDisable()
	{
		GameEvents.playerAction -= OnPlayerAction;
		GameEvents.setEnergyAmount -= OnSetEnergyAmount;
		GameEvents.playCredits -= OnPlayCredits;
	}

	private void OnPlayerAction(PlayerActions playerActions, int energy)
	{
		switch(playerActions)
		{
			case PlayerActions.jump:
				jump.Deplete(energy);
				break;
			case PlayerActions.hookshot:
				hookshot.Deplete(energy);
				break;
			case PlayerActions.moveLeft:
				moveLeft.Deplete(energy);
				break;
			case PlayerActions.moveRight:
				moveRight.Deplete(energy);
				break;
			default:
				break;
		}
	}

	private void OnSetEnergyAmount(PlayerActions playerActions, int energy)
	{
		switch (playerActions)
		{
			case PlayerActions.jump:
				jump.Recharge(energy);
				break;
			case PlayerActions.hookshot:
				hookshot.Recharge(energy);
				break;
			case PlayerActions.moveLeft:
				moveLeft.Recharge(energy);
				break;
			case PlayerActions.moveRight:
				moveRight.Recharge(energy);
				break;
			default:
				break;
		}
	}

	private void OnPlayCredits()
	{
		credits.SetActive(true);

		audioSource.clip = creditsClip;
		audioSource.Play();
	}
}