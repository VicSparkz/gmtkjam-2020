﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UseResource : MonoBehaviour
{
    public Image image;
    public Sprite sprite;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void Decrement()
    {
        if (image.overrideSprite != null)
        {
            image.overrideSprite = sprite;
        }
        else
        {
            image.overrideSprite = null;
        }
    }
}
